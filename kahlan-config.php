<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use DI\ContainerBuilder;
use Kahlan\Filter\Filters;

Filters::apply($this, 'run', function ($next) {
    $containerBuilder = new ContainerBuilder;
    $containerBuilder->addDefinitions(include __DIR__ . '/config/Di.php');
    $scope                   = $this->suite()->root()->scope(); // The top most describe scope.
    $scope->containerBuilder = $containerBuilder;

    return $next();
});
