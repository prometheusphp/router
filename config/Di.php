<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    \FastRoute\RouteParser::class                     => \DI\get(\FastRoute\RouteParser\Std::class),
    \Prometheus\Router\Router::class                  => \DI\autowire()
        ->constructorParameter('notFoundHandler', \DI\get(\Prometheus\Router\Handler\NotFound::class))
        ->constructorParameter('notAllowedHandler', \DI\get(\Prometheus\Router\Handler\MethodNotAllowed::class))
        ->constructorParameter('middleware', \DI\factory([
            \Prometheus\Router\Middleware\GroupFactory::class,
            'get'
        ])->parameter('id', \Prometheus\Router\Middleware\Group::class)),
    \Prometheus\Router\Middleware\GroupFactory::class => \DI\autowire()->constructorParameter('queueFactory', \DI\get(\Prometheus\Router\Middleware\QueueFactory::class)),
    \Prometheus\Router\Middleware\QueueFactory::class => \DI\autowire()
        ->constructorParameter('handlerFactory', \DI\get('handlerFactory'))
        ->constructorParameter('middlewareFactory', \DI\get('middlewareFactory')),
    \Prometheus\Router\RouteFactory::class            => \DI\autowire()
        ->constructorParameter('handlerFactory', \DI\get('handlerFactory'))
        ->constructorParameter('middlewareFactory', \DI\get(\Prometheus\Router\Middleware\GroupFactory::class)),
    'handlerFactory' => \DI\get(\Psr\Container\ContainerInterface::class),
    'middlewareFactory' => \DI\get(\Psr\Container\ContainerInterface::class),
    \Prometheus\Router\GroupFactoryInterface::class => \DI\get(\Prometheus\Router\GroupFactory::class),
    \Prometheus\Router\RouteFactoryInterface::class => \DI\get(\Prometheus\Router\RouteFactory::class),
    \Prometheus\Router\GroupFactory::class          => \DI\autowire()->constructorParameter('middlewareFactory', \DI\get(\Prometheus\Router\Middleware\GroupFactory::class)),

];