<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router;

use Prometheus\Router\Middleware\Group as MiddlewareGroup;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2018 Thomas Gnandt
 * Class GroupFactory
 */
class GroupFactory implements GroupFactoryInterface {

    /**
     * @var ContainerInterface
     */
    protected $middlewareFactory;

    /**
     * RouteFactory constructor.
     *
     * @param ContainerInterface $middlewareFactory
     */
    public function __construct(ContainerInterface $middlewareFactory) {
        $this->middlewareFactory = $middlewareFactory;
    }

    /**
     * Create group.
     *
     * @param Router $router
     * @param string $pattern
     *
     * @return Group
     * @throws NotFoundExceptionInterface  No entry was found for MiddlewareGroup.
     * @throws ContainerExceptionInterface Error while retrieving MiddlewareGroup.
     */
    public function createGroup(Router $router, string $pattern) {
        $group = new Group($router, $pattern, $this, $this->middlewareFactory->get(MiddlewareGroup::class));

        return $group;
    }

}
