<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router;

use Prometheus\Router\Middleware\AwareInterface;
use Psr\Http\Server\RequestHandlerInterface;

interface RouteInterface extends RequestHandlerInterface, AwareInterface {

    /**
     * Get route methods
     *
     * @return string[]
     */
    public function getMethods();

    /**
     * Get the route pattern
     *
     * @return string
     */
    public function getPattern();
}
