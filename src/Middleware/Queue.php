<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router\Middleware;

use Prometheus\Router\Traits\BuilderTrait;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2018 Thomas Gnandt
 * Class Queue
 */
class Queue implements QueueInterface {

    use BuilderTrait;

    /**
     * Factory to get handler
     *
     * @var ContainerInterface
     */
    protected $middlewareFactory;

    /**
     * Is this a clone of the original?
     * I.e. is this already dispatched?
     *
     * @var bool
     */
    protected $cloned = false;

    /**
     * Middlewares
     *
     * @var MiddlewareInterface[]
     */
    protected $middlewares = [];

    /**
     * Has this been dispatched?
     *
     * @var bool
     */
    protected $dispatched = false;

    /**
     * Queue constructor.
     *
     * @param ContainerInterface $handlerFactory
     * @param ContainerInterface $middlewareFactory
     */
    public function __construct(ContainerInterface $handlerFactory = null,
                                ContainerInterface $middlewareFactory = null) {
        $this->handlerFactory    = $handlerFactory;
        $this->middlewareFactory = $middlewareFactory;
    }

    /**
     * Add Middleware to queue.
     *
     * @param string|MiddlewareInterface $middleware
     *
     * @return static
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    public function addMiddleware($middleware) {
        if (true === $this->dispatched) {
            throw new \RuntimeException('Can not add middleware after dispatch.');
        }
        if (!is_string($middleware) && !($middleware instanceof MiddlewareInterface)) {
            $type = gettype($middleware);
            if ($type === 'object') {
                $type = 'Instance of ' . get_class($middleware);
            }
            $msg = sprintf('Middleware has to be a string or an instance of %s, %s given', MiddlewareInterface::class, $type);
            throw new \InvalidArgumentException($msg);
        }
        $this->middlewares[] = $middleware;

        return $this;
    }

    /**
     * Handle the request and return a response.
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     * @throws NotFoundExceptionInterface  No entry was found for handler or middleware.
     * @throws ContainerExceptionInterface Error while retrieving handler or middleware.
     */
    public function handle(ServerRequestInterface $request): ResponseInterface {
        $this->dispatched = true;
        $this->buildHandler();
        if ($this->middlewares && !$this->cloned) {
            $this->buildMiddlewares();
            $handler = clone $this;

            return $handler->handle($request);
        } elseif ($this->middlewares) {
            $middleware = array_shift($this->middlewares);

            return $middleware->process($request, $this);
        }

        return $this->handler->handle($request);
    }

    /**
     * Build all middlewares
     * @throws \RuntimeException
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     */
    protected function buildMiddlewares() {
        foreach ($this->middlewares as &$middleware) {
            $middleware = $this->build($middleware, $this->middlewareFactory, MiddlewareInterface::class, 'Middleware');
        }
    }

    /**
     * @param RequestHandlerInterface|string $handler
     *
     * @throws \InvalidArgumentException
     */
    public function setHandler($handler) {
        $this->validateHandler($handler);
        $this->handler = $handler;
    }

    /**
     * Set cloned flag on clone
     */
    protected function __clone() {
        $this->cloned = true;
    }

}
