<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router\Middleware;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * A list of middlewares that is itself a middleware
 *
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2018 Thomas Gnandt
 */
class Group implements CapableInterface {

    /**
     * @var bool
     */
    protected $hasMiddlewares = false;

    /**
     * @var RequestHandlerInterface
     */
    protected $queue;

    /**
     * @var ContainerInterface
     */
    protected $queueFactory;

    /**
     * Middleware constructor.
     *
     * @param ContainerInterface $queueFactory
     */
    public function __construct(ContainerInterface $queueFactory) {
        $this->queueFactory = $queueFactory;
    }

    /**
     * Add Middleware to queue.
     *
     * @param string|MiddlewareInterface $middleware
     *
     * @return static
     * @throws NotFoundExceptionInterface  No entry was found for Queue.
     * @throws ContainerExceptionInterface Error while retrieving the Queue.
     */
    public function addMiddleware($middleware) {
        $this->getQueue()->addMiddleware($middleware);
        $this->hasMiddlewares = true;

        return $this;
    }

    /**
     * @return QueueInterface
     * @throws NotFoundExceptionInterface  No entry was found for Queue.
     * @throws ContainerExceptionInterface Error while retrieving the Queue.
     */
    protected function getQueue(): QueueInterface {
        if (null === $this->queue) {
            $this->queue = $this->queueFactory->get(Queue::class);
        }

        return $this->queue;
    }

    /**
     * {@inheritdoc}
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        if ($this->hasMiddlewares) {
            $queue = $this->getQueue();
            $queue->setHandler($handler);

            return $queue->handle($request);
        }

        return $handler->handle($request);
    }

}
