<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router\Middleware;

use Psr\Http\Server\MiddlewareInterface;

interface CapableInterface extends MiddlewareInterface {

    /**
     * Add Middleware to queue.
     *
     * @param string|MiddlewareInterface $middleware
     *
     * @return $this
     */
    public function addMiddleware($middleware);
}
