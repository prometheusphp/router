<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router\Middleware;

use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2018 Thomas Gnandt
 * Class Queue
 */
interface QueueInterface extends RequestHandlerInterface, AwareInterface {
    /**
     * @param RequestHandlerInterface|string $handler
     */
    public function setHandler($handler);
}
