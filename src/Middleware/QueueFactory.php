<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router\Middleware;

use Prometheus\Router\Exception\NotFound;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2018 Thomas Gnandt
 * Class QueueFactory
 */
class QueueFactory implements ContainerInterface {

    /**
     * @var ContainerInterface
     */
    protected $handlerFactory;

    /**
     * @var ContainerInterface
     */
    protected $middlewareFactory;

    /**
     * RouteFactory constructor.
     *
     * @param ContainerInterface|null $handlerFactory
     * @param ContainerInterface|null $middlewareFactory
     */
    public function __construct(ContainerInterface $handlerFactory = null,
                                ContainerInterface $middlewareFactory = null) {
        $this->handlerFactory    = $handlerFactory;
        $this->middlewareFactory = $middlewareFactory;
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     *
     * @return mixed Entry.
     */
    public function get($id) {
        if ($id !== Queue::class) {
            throw new NotFound(sprintf('Unknown identifier %s', $id));
        }

        return new Queue($this->handlerFactory, $this->middlewareFactory);
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has($id) {
        if ($id === Queue::class) {
            return true;
        }

        return false;
    }

}
