<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router;

use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use FastRoute\RouteParser;
use Prometheus\Router\Exception\Route as RouteException;
use Prometheus\Router\Middleware\AwareInterface;
use Prometheus\Router\Middleware\AwareTrait;
use Prometheus\Router\Middleware\CapableInterface;
use Prometheus\Router\Traits\RouteGenerationTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 * Class Router
 */
class Router implements RequestHandlerInterface, RouteGenerationInterface, AwareInterface {

    use RouteGenerationTrait;
    use AwareTrait;

    /**
     * @var RouteInterface[]
     */
    protected $routes = [];

    /**
     * @var RouteFactoryInterface
     */
    protected $routeFactory;

    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * @var RouteParser
     */
    protected $routeParser;

    /**
     * @var GroupFactoryInterface
     */
    protected $groupFactory;

    /**
     * @var RequestHandlerInterface
     */
    protected $notAllowedHandler;

    /**
     * @var RequestHandlerInterface
     */
    protected $notFoundHandler;

    /**
     * @var CapableInterface
     */
    protected $middleware;

    /**
     * Router constructor.
     *
     * @param RouteFactoryInterface $routeFactory
     * @param RouteParser $routeParser
     * @param GroupFactoryInterface $groupFactory
     * @param RequestHandlerInterface $notAllowedHandler
     * @param RequestHandlerInterface $notFoundHandler
     * @param CapableInterface $middleware
     */
    public function __construct(RouteFactoryInterface $routeFactory, RouteParser $routeParser,
                                GroupFactoryInterface $groupFactory, RequestHandlerInterface $notAllowedHandler,
                                RequestHandlerInterface $notFoundHandler, CapableInterface $middleware) {
        $this->routeFactory      = $routeFactory;
        $this->routeParser       = $routeParser;
        $this->groupFactory      = $groupFactory;
        $this->notAllowedHandler = $notAllowedHandler;
        $this->notFoundHandler   = $notFoundHandler;
        $this->middleware        = $middleware;
    }

    /**
     * @param string[] $methods List of HTTP methods
     * @param string $pattern The route pattern
     * @param string|RequestHandlerInterface $handler The route handler
     * @param string|null $name The route's name
     *
     * @return RouteInterface
     * @throws RouteException
     */
    public function map(array $methods, string $pattern, $handler, string $name = null): RouteInterface {
        // Add route
        $route = $this->routeFactory->createRoute($methods, $pattern, $handler);
        $route->addMiddleware($this->middleware);
        if ($name) {
            if (isset($this->routes[ $name ])) {
                throw new RouteException(sprintf('Route-name %s already in use', $name));
            }
            $this->routes[ $name ] = $route;
        } else {
            $this->routes[] = $route;
        }

        return $route;
    }

    /**
     * {@inheritdoc}
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface {
        $uri = '/' . ltrim($request->getUri()->getPath(), '/');

        $routeInfo = $this->createDispatcher()->dispatch($request->getMethod(), $uri);
        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                $response = $this->notFoundHandler->handle($request);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $request = $request->withAttribute('allowedMethods', $routeInfo[1]);

                $response = $this->notAllowedHandler->handle($request);
                break;
            case Dispatcher::FOUND:
                /**@var $handler RequestHandlerInterface */
                $handler    = $routeInfo[1];
                $attributes = $routeInfo[2];
                foreach ($attributes as $key => $value) {
                    $request = $request->withAttribute($key, $value);
                }

                $response = $handler->handle($request);
                break;
        }

        return $response;
    }

    /**
     * @return Dispatcher
     */
    protected function createDispatcher() {
        if ($this->dispatcher) {
            return $this->dispatcher;
        }

        $definitionCallback = function (RouteCollector $collector) {
            foreach ($this->routes as $route) {
                $collector->addRoute($route->getMethods(), $route->getPattern(), $route);
            }
        };

        $this->dispatcher = \FastRoute\simpleDispatcher($definitionCallback, [
            'routeParser' => $this->routeParser
        ]);

        return $this->dispatcher;
    }

    /**
     * Create group
     *
     * @param string $pattern
     * @param callable|null $callback
     *
     * @return Group
     */
    public function group(string $pattern, callable $callback = null) {
        $group = $this->groupFactory->createGroup($this, $pattern);
        if ($callback) {
            $callback($group);
        }

        return $group;
    }

    /**
     * Build the path for a named route
     *
     * @param string $name Route name
     * @param array $data Named argument replacement data
     *
     * @return string
     *
     * @throws RouteException         If named route does not exist
     * @throws \InvalidArgumentException If required data not provided
     */
    public function pathFor($name, array $data = []) {
        if (!isset($this->routes[ $name ])) {
            throw new RouteException('Named route does not exist for name: ' . $name);
        }
        $route   = $this->routes[ $name ];
        $pattern = $route->getPattern();

        $routeDatas = $this->routeParser->parse($pattern);
        // $routeDatas is an array of all possible routes that can be made. There is
        // one routeData for each optional parameter plus one for no optional parameters.
        //
        // The most specific is last, so we look for that first.
        $routeDatas = array_reverse($routeDatas);

        $segments = [];
        foreach ($routeDatas as $routeData) {
            foreach ($routeData as $item) {
                if (is_string($item)) {
                    // this segment is a static string
                    $segments[] = $item;
                    continue;
                }

                // This segment has a parameter: first element is the name
                if (!array_key_exists($item[0], $data)) {
                    // we don't have a data element for this segment: cancel
                    // testing this routeData item, so that we can try a less
                    // specific routeData item.
                    $segments    = [];
                    $segmentName = $item[0];
                    break;
                }
                $segments[] = $data[ $item[0] ];
            }
            if (!empty($segments)) {
                // we found all the parameters for this route data, no need to check
                // less specific ones
                break;
            }
        }

        if (empty($segments)) {
            throw new \InvalidArgumentException('Missing data for URL segment: ' . $segmentName);
        }
        $url = implode('', $segments);

        return $url;
    }

}
