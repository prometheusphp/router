<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router;

use Prometheus\Router\Middleware\AwareTrait;
use Prometheus\Router\Middleware\CapableInterface;
use Prometheus\Router\Traits\BuilderTrait;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 * Class Route
 */
class Route implements RouteInterface {

    use AwareTrait;
    use BuilderTrait;

    /**
     * HTTP methods supported by this route
     *
     * @var string[]
     */
    protected $methods = [];

    /**
     * @var string
     */
    protected $pattern;

    /**
     * Route constructor.
     *
     * @param string[] $methods The route HTTP methods
     * @param string $pattern The route pattern
     * @param RequestHandlerInterface|string $handler
     * @param CapableInterface $middleware
     * @param ContainerInterface $handlerFactory
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $methods, string $pattern, $handler, CapableInterface $middleware,
                                ContainerInterface $handlerFactory = null) {
        $this->validateHandler($handler);
        // According to RFC methods are defined in uppercase (See RFC 7231)
        $this->methods        = array_map("strtoupper", $methods);
        $this->pattern        = $pattern;
        $this->handler        = $handler;
        $this->middleware     = $middleware;
        $this->handlerFactory = $handlerFactory;
    }

    /**
     * {@inheritdoc}
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface {
        $this->buildHandler();

        return $this->middleware->process($request, $this->handler);
    }

    /**
     * Get route methods
     *
     * @return string[]
     */
    public function getMethods() {
        return $this->methods;
    }

    /**
     * Get the route pattern
     *
     * @return string
     */
    public function getPattern() {
        return $this->pattern;
    }

}
