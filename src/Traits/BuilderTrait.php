<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router\Traits;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

trait BuilderTrait {

    /**
     * @var string|RequestHandlerInterface
     */
    protected $handler;

    /**
     * @var ContainerInterface
     */
    protected $handlerFactory;

    /**
     * Build handler
     *
     * @throws \RuntimeException
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     */
    protected function buildHandler() {
        $this->handler = $this->build($this->handler, $this->handlerFactory, RequestHandlerInterface::class, 'Handler');
    }

    /**
     * Build a handler or middleware
     *
     * @param $id
     * @param ContainerInterface|null $factory
     * @param string $className
     * @param string $label
     *
     * @return MiddlewareInterface|RequestHandlerInterface
     * @throws \RuntimeException test
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     */
    protected function build($id, ContainerInterface $factory = null, string $className, string $label) {
        if (is_string($id) && $factory !== null) {
            $id = $factory->get($id);
        }
        if (!$id instanceof $className) {
            $type = gettype($id);
            if ($type === 'object') {
                $type = 'instance of ' . get_class($id);
            }
            throw new \RuntimeException(sprintf('%s was not resolved to an instance of %s, %s given', $label, $className, $type));
        }

        return $id;
    }

    /**
     * Validate handler is string or RequestHandlerInterface.
     * Throws \InvalidArgumentException if not.
     *
     * @param $handler
     *
     * @throws \InvalidArgumentException
     */
    protected function validateHandler($handler) {
        if (!is_string($handler) && !($handler instanceof RequestHandlerInterface)) {
            $type = gettype($handler);
            if ($type === 'object') {
                $type = 'Instance of ' . get_class($handler);
            }
            $msg = sprintf('Handler has to be a string or an instance of %s, %s given', RequestHandlerInterface::class, $type);
            throw new \InvalidArgumentException($msg);
        }
    }
}
