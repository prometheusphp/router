<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router\Traits;

use Prometheus\Router\RouteInterface;
use Psr\Http\Server\RequestHandlerInterface;

trait RouteGenerationTrait {

    /**
     * Add GET route
     *
     * @param string $pattern The route URI pattern
     * @param RequestHandlerInterface|string $handler The route handler
     * @param string $name The route's name
     *
     * @return RouteInterface
     */
    public function get(string $pattern, $handler, string $name = null): RouteInterface {
        return $this->map(['GET'], $pattern, $handler, $name);
    }

    /**
     * Add POST route
     *
     * @param string $pattern The route URI pattern
     * @param RequestHandlerInterface|string $handler The route handler
     * @param string $name The route's name
     *
     * @return RouteInterface
     */
    public function post(string $pattern, $handler, string $name = null): RouteInterface {
        return $this->map(['POST'], $pattern, $handler, $name);
    }

    /**
     * Add PUT route
     *
     * @param string $pattern The route URI pattern
     * @param RequestHandlerInterface|string $handler The route handler
     * @param string $name The route's name
     *
     * @return RouteInterface
     */
    public function put(string $pattern, $handler, string $name = null): RouteInterface {
        return $this->map(['PUT'], $pattern, $handler, $name);
    }

    /**
     * Add PATCH route
     *
     * @param string $pattern The route URI pattern
     * @param RequestHandlerInterface|string $handler The route handler
     * @param string $name The route's name
     *
     * @return RouteInterface
     */
    public function patch(string $pattern, $handler, string $name = null): RouteInterface {
        return $this->map(['PATCH'], $pattern, $handler, $name);
    }

    /**
     * Add DELETE route
     *
     * @param string $pattern The route URI pattern
     * @param RequestHandlerInterface|string $handler The route handler
     * @param string $name The route's name
     *
     * @return RouteInterface
     */
    public function delete(string $pattern, $handler, string $name = null): RouteInterface {
        return $this->map(['DELETE'], $pattern, $handler, $name);
    }

    /**
     * Add OPTIONS route
     *
     * @param string $pattern The route URI pattern
     * @param RequestHandlerInterface|string $handler The route handler
     * @param string $name The route's name
     *
     * @return RouteInterface
     */
    public function options(string $pattern, $handler, string $name = null): RouteInterface {
        return $this->map(['OPTIONS'], $pattern, $handler, $name);
    }

    /**
     * Add route for any HTTP method
     *
     * @param string $pattern The route URI pattern
     * @param RequestHandlerInterface|string $handler The route handler
     * @param string $name The route's name
     *
     * @return RouteInterface
     */
    public function any(string $pattern, $handler, string $name = null): RouteInterface {
        return $this->map(['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'], $pattern, $handler, $name);
    }

    /**
     * Add route
     *
     * @param string[] $methods List of HTTP methods
     * @param string $pattern The route pattern
     * @param string|RequestHandlerInterface $handler The route handler
     * @param string $name The route's name
     *
     * @return RouteInterface
     */
    abstract public function map(array $methods, string $pattern, $handler, string $name = null): RouteInterface;

}
