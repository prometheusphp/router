<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2018 Thomas Gnandt
 * Class NotFound
 */
class MethodNotAllowed implements RequestHandlerInterface {

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * NotFound constructor.
     *
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response) {
        $this->response = $response;
    }

    /**
     * {@inheritdoc}
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface {
        return $this->response->withStatus(405)
                              ->withAddedHeader('Allow', implode(', ', (array) $request->getAttribute('allowedMethods')));
    }
}
