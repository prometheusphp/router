<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router;

interface GroupFactoryInterface {

    /**
     * Create group.
     *
     * @param Router $router
     * @param string $pattern
     *
     * @return Group
     */
    public function createGroup(Router $router, string $pattern);
}
