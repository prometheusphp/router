<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router;

use Prometheus\Router\Middleware\Group;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 * Class Route
 */
class RouteFactory implements RouteFactoryInterface {

    /**
     * @var ContainerInterface
     */
    protected $handlerFactory;

    /**
     * @var ContainerInterface
     */
    protected $middlewareFactory;

    /**
     * RouteFactory constructor.
     *
     * @param ContainerInterface|null $handlerFactory
     * @param ContainerInterface $middlewareFactory
     */
    public function __construct(ContainerInterface $middlewareFactory, ContainerInterface $handlerFactory = null) {
        $this->middlewareFactory = $middlewareFactory;
        $this->handlerFactory    = $handlerFactory;
    }

    /**
     * Create a new Route object
     *
     * @param  string[] $methods List of HTTP methods
     * @param  string $pattern The route pattern
     * @param  string|RequestHandlerInterface $handler The route callable
     *
     * @return RouteInterface
     * @throws NotFoundExceptionInterface  No entry was found for group.
     * @throws ContainerExceptionInterface Error while retrieving group.
     */
    public function createRoute(array $methods, string $pattern, $handler): RouteInterface {
        $group = $this->middlewareFactory->get(Group::class);
        $route = new Route($methods, $pattern, $handler, $group, $this->handlerFactory);

        return $route;
    }
}
