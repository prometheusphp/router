<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router\Exception;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 * Class Route
 */
class Route extends \Exception {

}
