<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router;

interface RouteFactoryInterface {

    /**
     * Create a new Route object
     *
     * @param  string[] $methods List of HTTP methods
     * @param  string $pattern The route pattern
     * @param  callable $handler The route callable
     *
     * @return RouteInterface
     */
    public function createRoute(array $methods, string $pattern, $handler): RouteInterface;
}
