<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Prometheus\Router;

use Prometheus\Router\Exception\Route;
use Prometheus\Router\Middleware\AwareInterface;
use Prometheus\Router\Middleware\AwareTrait;
use Prometheus\Router\Middleware\CapableInterface;
use Prometheus\Router\Traits\RouteGenerationTrait;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author    Thomas Gnandt <prometheusphp@gmail.com>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 * Class Group
 */
class Group implements AwareInterface, RouteGenerationInterface {

    use AwareTrait, RouteGenerationTrait;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var string
     */
    protected $pattern;

    /**
     * @var GroupFactoryInterface
     */
    protected $groupFactory;

    /**
     * @var CapableInterface
     */
    protected $middleware;

    /**
     * Group constructor.
     *
     * @param Router $router
     * @param string $pattern
     * @param GroupFactoryInterface $groupFactory
     * @param CapableInterface $middleware
     */
    public function __construct(Router $router, $pattern, GroupFactoryInterface $groupFactory,
                                CapableInterface $middleware) {
        $this->router       = $router;
        $this->pattern      = $pattern;
        $this->groupFactory = $groupFactory;
        $this->middleware   = $middleware;
    }

    /**
     * Add route
     *
     * @param string[] $methods List of HTTP methods
     * @param string $pattern The route pattern
     * @param string|RequestHandlerInterface $handler The route handler
     * @param string $name The route's name
     *
     * @return RouteInterface
     * @throws Route
     */
    public function map(array $methods, string $pattern, $handler, string $name = null): RouteInterface {
        $route = $this->router->map($methods, $this->pattern . $pattern, $handler, $name);
        $route->addMiddleware($this->middleware);

        return $route;
    }

    public function group(string $pattern, callable $callback = null) {
        $group = $this->groupFactory->createGroup($this->router, $this->pattern . $pattern);
        $group->getMiddleware()->addMiddleware($this->middleware);
        if ($callback) {
            $callback($group);
        }

        return $group;
    }

    /**
     * @return CapableInterface
     */
    public function getMiddleware(): CapableInterface {
        return $this->middleware;
    }

}
