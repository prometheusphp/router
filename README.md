# Prometheus Router
[![pipeline status](https://gitlab.com/prometheusphp/router/badges/master/pipeline.svg)](https://gitlab.com/prometheusphp/router/commits/master)
[![coverage report](https://gitlab.com/prometheusphp/router/badges/master/coverage.svg)](https://tgitlab.com/prometheusphp/router/commits/master)

A simple PSR-15 compatible Router.

