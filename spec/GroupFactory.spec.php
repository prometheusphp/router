<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Prometheus\Router\GroupFactory;

describe(GroupFactory::class, function () {
    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $containerBuilder->addDefinitions([
            \Psr\Http\Message\ResponseInterface::class => \DI\get(\Zend\Diactoros\Response\EmptyResponse::class),
        ]);
        $this->di = $containerBuilder->build();
        $this->di->set('handlerFactory', $this->di);
        $this->di->set('middlewareFactory', $this->di);
    });
    beforeEach(function () {
        $this->instance = $this->di->get(GroupFactory::class);
    });
    it('implements the interface', function () {
        expect($this->instance)->toBeAnInstanceOf(\Prometheus\Router\GroupFactoryInterface::class);
    });
    it('creates Group', function () {
        $router = $this->di->get(\Prometheus\Router\Router::class);
        expect($this->di->get(\Prometheus\Router\Middleware\GroupFactory::class))->toReceive('get');
        $group = $this->instance->createGroup($router, '/test');
        expect($group)->toBeAnInstanceOf(\Prometheus\Router\Group::class);
        expect($group->getMiddleware())->toBeAnInstanceOf(\Prometheus\Router\Middleware\Group::class);
        $reflection = new ReflectionClass($group);
        $property   = $reflection->getProperty('pattern');
        $property->setAccessible(true);
        expect($property->getValue($group))->toBe('/test');
        $property = $reflection->getProperty('router');
        $property->setAccessible(true);
        expect($property->getValue($group))->toBe($router);
        $property = $reflection->getProperty('groupFactory');
        $property->setAccessible(true);
        expect($property->getValue($group))->toBe($this->instance);
    });
});