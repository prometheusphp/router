<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Prometheus\Router\Handler\MethodNotAllowed;

describe(MethodNotAllowed::class, function () {
    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $containerBuilder->addDefinitions([
            MethodNotAllowed::class => \DI\autowire()->constructorParameter('response', new \Zend\Diactoros\Response\EmptyResponse())
        ]);
        $this->di      = $containerBuilder->build();
        $this->request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([], [], [], [], []);
    });
    beforeEach(function () {
        $this->instance = $this->di->get(MethodNotAllowed::class);
    });
    it('implements ' . \Psr\Http\Server\RequestHandlerInterface::class, function () {
        expect($this->instance)->toBeAnInstanceOf(\Psr\Http\Server\RequestHandlerInterface::class);
    });
    it('returns response with correct status code and header', function () {
        $request  = $this->request->withAttribute('allowedMethods', ['GET', 'POST']);
        $response = $this->instance->handle($request);
        expect($response->getStatusCode())->toBe(405);
        expect($response->getHeaderLine('Allow'))->toBe('GET, POST');
        $response = $this->instance->handle($this->request);
        expect($response->getHeaderLine('Allow'))->toBe('');
    });
});