<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Prometheus\Router\Handler\NotFound;

describe(NotFound::class, function () {
    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $containerBuilder->addDefinitions([
            NotFound::class => \DI\autowire()->constructorParameter('response', new \Zend\Diactoros\Response\EmptyResponse())
        ]);
        $this->di      = $containerBuilder->build();
        $this->request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([], [], [], [], []);
    });
    beforeEach(function () {
        $this->instance = $this->di->get(NotFound::class);
    });
    it('implements ' . \Psr\Http\Server\RequestHandlerInterface::class, function () {
        expect($this->instance)->toBeAnInstanceOf(\Psr\Http\Server\RequestHandlerInterface::class);
    });
    it('returns response with correct status code', function () {
        $response = $this->instance->handle($this->request);
        expect($response->getStatusCode())->toBe(404);
    });
});