<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Kahlan\Plugin\Double;
use Prometheus\Router\Route;
use Prometheus\Router\RouteFactory;
use Psr\Http\Server\RequestHandlerInterface;

describe(Route::class, function () {

    beforeAll(function () {
        $containerBuilder        = clone($this->containerBuilder);
        $this->di                = $containerBuilder->build();
        $this->request           = \Zend\Diactoros\ServerRequestFactory::fromGlobals([], [], [], [], []);
        $this->handler           = Double::instance(['implements' => RequestHandlerInterface::class]);
        $this->routeFactory      = $this->di->get(RouteFactory::class);
        $this->middlewareFactory = $this->di->get(\Prometheus\Router\Middleware\GroupFactory::class);
    });
    beforeEach(function () {
        allow($this->handler)->toReceive('handle')
                             ->andReturn(Double::instance(['implements' => \Psr\Http\Message\ResponseInterface::class]));
    });
    it('implements interfaces', function () {
        $instance = $this->routeFactory->createRoute(['GET'], 'pattern', '');
        expect($instance)->toBeAnInstanceOf(RequestHandlerInterface::class);
        expect($instance)->toBeAnInstanceOf(\Prometheus\Router\RouteInterface::class);
        expect($instance)->toBeAnInstanceOf(\Prometheus\Router\Middleware\AwareInterface::class);
    });
    it('errors on invalid handler', function () {
        $handler = new StdClass();
        $name    = 'test';
        $this->di->set($name, $handler);
        $routeFactory = new RouteFactory($this->middlewareFactory, $this->di);
        $request      = $this->request;
        $closure      = function () use ($routeFactory, $request, $handler) {
            $routeFactory->createRoute(['GET'], 'pattern', $handler);
        };
        expect($closure)->toThrow(new InvalidArgumentException(sprintf('Handler has to be a string or an instance of %s, Instance of stdClass given', RequestHandlerInterface::class)));
    });
    describe('::getMethods', function () {
        it('returns method list', function () {
            $list     = ['GET', 'POST'];
            $instance = $this->routeFactory->createRoute($list, 'pattern', $this->handler);
            expect($instance->getMethods())->toBe($list);
        });
        it('capitalizes methods', function () {
            $list     = ['get', 'post', 'anything'];
            $instance = $this->routeFactory->createRoute($list, 'pattern', $this->handler);
            expect($instance->getMethods())->toBe(['GET', 'POST', 'ANYTHING']);
        });
    });
    describe('::getPattern', function () {
        it('returns patter', function () {
            $pattern  = 'anything';
            $instance = $this->routeFactory->createRoute(['GET'], $pattern, $this->handler);
            expect($instance->getPattern())->toBe($pattern);
        });
    });
    describe('::addMiddleware', function () {
        it('adds middleware to group', function () {
            $group    = $this->middlewareFactory->get(\Prometheus\Router\Middleware\Group::class);
            $instance = new Route(['GET'], 'pattern', $this->handler, $group);
            expect($group)->toReceive('addMiddleware')->with('test');
            expect($instance->addMiddleware('test'))->toBe($instance);
        });
    });
    describe('::handle', function () {
        it('handles request', function () {
            $instance = $this->routeFactory->createRoute(['GET'], 'pattern', $this->handler);
            expect($this->handler)->toReceive('handle')->with($this->request);
            $result = $instance->handle($this->request);
            expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
        });
        it('uses HandlerFactory to resolve handler', function () {
            $name = 'test';
            $this->di->set($name, $this->handler);
            $routeFactory = new RouteFactory($this->middlewareFactory, $this->di);
            $instance     = $routeFactory->createRoute(['GET'], 'pattern', $name);
            expect($this->handler)->toReceive('handle')->with($this->request);
            $result = $instance->handle($this->request);
            expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
        });
        it('errors on invalid handler', function () {
            $handler = new StdClass();
            $name    = 'test';
            $name2   = 'test2';
            $this->di->set($name, $handler);
            $this->di->set($name2, 'anything');
            $routeFactory = new RouteFactory($this->middlewareFactory, $this->di);
            $instance     = $routeFactory->createRoute(['GET'], 'pattern', $name);
            $request      = $this->request;
            $closure      = function () use ($instance, $request) {
                return $instance->handle($request);
            };
            expect($closure)->toThrow(new \RuntimeException('Handler was not resolved to an instance of Psr\\Http\\Server\\RequestHandlerInterface, instance of stdClass given'));
            $instance = $this->routeFactory->createRoute(['GET'], 'pattern', $name2);
            $closure  = function () use ($instance, $request) {
                return $instance->handle($request);
            };
            expect($closure)->toThrow(new \RuntimeException('Handler was not resolved to an instance of Psr\\Http\\Server\\RequestHandlerInterface, string given'));
        });
        it('uses group to handle middleware', function () {
            $group    = $this->middlewareFactory->get(\Prometheus\Router\Middleware\Group::class);
            $instance = new Route(['GET'], 'pattern', $this->handler, $group);
            expect($group)->toReceive('process')->with($this->request, $this->handler);
            $instance->handle($this->request);
        });
    });
});