<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Kahlan\Plugin\Double;
use Prometheus\Router\Group;

describe(Group::class, function () {

    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $this->handler    = Double::instance(['implements' => \Psr\Http\Server\RequestHandlerInterface::class]);
        $containerBuilder->addDefinitions([
            \Psr\Http\Message\ResponseInterface::class => \DI\get(\Zend\Diactoros\Response\EmptyResponse::class),
            //            \Zend\Diactoros\Response\EmptyResponse::class => \DI\autowire()->scope(\DI\Scope::PROTOTYPE),
            //            'handler'                                     => $this->handler
        ]);
        $this->di = $containerBuilder->build();
        //        $this->di->set('handlerFactory', $this->di);
        //        $this->notFoundHandler   = $this->di->get(\Prometheus\Router\Handler\NotFound::class);
        //        $this->notAllowedHandler = $this->di->get(\Prometheus\Router\Handler\MethodNotAllowed::class);
        $this->groupFactory      = $this->di->get(\Prometheus\Router\GroupFactory::class);
        $this->middlewareFactory = $this->di->get(\Prometheus\Router\Middleware\GroupFactory::class);
        $this->router            = $this->di->get(\Prometheus\Router\Router::class);
    });
    beforeEach(function () {
        //        allow($this->handler)->toReceive('handle')
        //                             ->andReturn(Double::instance(['implements' => ResponseInterface::class]));
        //        $this->instance = $this->di->get(Router::class);
    });
    it('implements interfaces', function () {
        $instance = $this->groupFactory->createGroup($this->router, 'pattern');
        expect($instance)->toBeAnInstanceOf(\Prometheus\Router\Middleware\AwareInterface::class);
        expect($instance)->toBeAnInstanceOf(\Prometheus\Router\RouteGenerationInterface::class);
    });
    describe('::addMiddleware', function () {
        it('adds middleware to group', function () {
            $group    = $this->middlewareFactory->get(\Prometheus\Router\Middleware\Group::class);
            $instance = new Group($this->router, 'pattern', $this->groupFactory, $group);
            expect($group)->toReceive('addMiddleware')->with('test');
            expect($instance->addMiddleware('test'))->toBe($instance);
        });
    });
    describe('::getMiddleware', function () {
        it('returns middleware-group', function () {
            $group    = $this->middlewareFactory->get(\Prometheus\Router\Middleware\Group::class);
            $instance = new Group($this->router, 'pattern', $this->groupFactory, $group);
            expect($instance->getMiddleware())->toBe($group);
        });
    });
    describe('::group', function () {
        it('creates group using factory', function () {
            $subGroup = $this->groupFactory->createGroup($this->router, 'pattern1pattern2');
            allow($this->groupFactory)->toReceive('createGroup')->with($this->router, 'pattern1pattern2')
                                      ->andReturn($subGroup);
            expect($this->groupFactory)->toReceive('createGroup')->with($this->router, 'pattern1pattern2');
            $group    = $this->middlewareFactory->get(\Prometheus\Router\Middleware\Group::class);
            $instance = new Group($this->router, 'pattern1', $this->groupFactory, $group);
            expect($subGroup->getMiddleware())->toReceive('addMiddleware')->with($group);
            expect($instance->group('pattern2'))->toBe($subGroup);
        });
        it('executes callback', function () {
            $subGroup = $this->groupFactory->createGroup($this->router, '');
            allow($this->groupFactory)->toReceive('createGroup')->andReturn($subGroup);
            $instance = $this->groupFactory->createGroup($this->router, '');
            $test     = false;
            $callback = function ($group) use ($subGroup, &$test) {
                expect($group)->toBe($subGroup);
                $test = true;
            };
            expect($instance->group('pattern2', $callback))->toBe($subGroup);
            expect($test)->toBe(true);
        });
    });
    describe('::HTTP Methods', function () {
        beforeAll(function () {
            $this->group    = $this->middlewareFactory->get(\Prometheus\Router\Middleware\Group::class);
            $this->instance = new Group($this->router, 'pattern1', $this->groupFactory, $this->group);
        });
        describe('::map', function () {
            it('creates route', function () {
                expect($this->instance->map(['GET'], '/pattern2', $this->handler))->toBeAnInstanceOf(\Prometheus\Router\RouteInterface::class);
            });
            it('proxies router map', function () {
                $route = $this->router->map(['GET', 'POST'], '/pattern2', $this->handler);
                allow($this->router)->toReceive('map')->andReturn($route);
                expect($this->router)->toReceive('map')->with([
                    'GET',
                    'POST'
                ], 'pattern1/pattern2', $this->handler, 'name');
                $this->instance->map(['GET', 'POST'], '/pattern2', $this->handler, 'name');
            });
            it('adds middleware', function () {
                $route = $this->router->map(['GET', 'POST'], '/pattern2', $this->handler);
                allow($this->router)->toReceive('map')->andReturn($route);
                expect($route)->toReceive('addMiddleware')->with($this->group);
                $this->instance->map(['GET', 'POST'], '/pattern2', $this->handler);
            });

        });
        describe('::get', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['GET'], '/pattern2', $this->handler);
                $this->instance->get('/pattern2', $this->handler);
            });
        });
        describe('::post', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['POST'], '/pattern2', $this->handler);
                $this->instance->post('/pattern2', $this->handler);
            });
        });
        describe('::put', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['PUT'], '/pattern2', $this->handler);
                $this->instance->put('/pattern2', $this->handler);
            });
        });
        describe('::patch', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['PATCH'], '/pattern2', $this->handler);
                $this->instance->patch('/pattern2', $this->handler);
            });
        });
        describe('::delete', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['DELETE'], '/pattern2', $this->handler);
                $this->instance->delete('/pattern2', $this->handler);
            });
        });
        describe('::options', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['OPTIONS'], '/pattern2', $this->handler);
                $this->instance->options('/pattern2', $this->handler);
            });
        });
        describe('::any', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with([
                    'GET',
                    'POST',
                    'PUT',
                    'PATCH',
                    'DELETE',
                    'OPTIONS'
                ], '/pattern2', $this->handler);
                $this->instance->any('/pattern2', $this->handler);
            });
        });
    });
});