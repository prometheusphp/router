<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Prometheus\Router\Router;

describe('integration', function () {

    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $containerBuilder->addDefinitions([
            Router::class                                 => \DI\autowire()
                ->constructorParameter('notFoundHandler', \DI\get(\Prometheus\Router\Handler\NotFound::class))
                ->constructorParameter('notAllowedHandler', \DI\get(\Prometheus\Router\Handler\MethodNotAllowed::class))
                ->constructorParameter('middleware', \DI\factory([
                    \Prometheus\Router\Middleware\GroupFactory::class,
                    'get'
                ])->parameter('id', \Prometheus\Router\Middleware\Group::class))->scope(\DI\Scope::SINGLETON()),
            \Psr\Http\Message\ResponseInterface::class    => \DI\get(\Zend\Diactoros\Response\EmptyResponse::class),
            \Zend\Diactoros\Response\EmptyResponse::class => \DI\autowire()->scope(\DI\Scope::PROTOTYPE),
        ]);
        $this->di = $containerBuilder->build();
        $this->di->set('handlerFactory', $this->di);
        $this->di->set('middlewareFactory', $this->di);
        $this->notFoundHandler   = $this->di->get(\Prometheus\Router\Handler\NotFound::class);
        $this->notAllowedHandler = $this->di->get(\Prometheus\Router\Handler\MethodNotAllowed::class);
        $this->instance          = $this->di->get(Router::class);
        $this->instance->addMiddleware('router.middleware1')
                       ->addMiddleware('router.middleware2')//            ->addMiddleware('router.middleware3')->addMiddleware('router.middleware4')
                       ->group('/group1', function ($group) {
                $group->addMiddleware('group1.middleware1');
                $group->get('/route1', 'group1.route1.GET', 'group1.route1.GET');
                $group->get('/route2', 'group1.route2.GET', 'group1.route2.GET');
                $group->post('/route2', 'group1.route2.POST');
                $group->addMiddleware('group1.middleware2');
                $subgroup = $group->group('/subgroup1');
                $subgroup->post('/route1', 'group1.subgroup1.route1.POST', 'group1.subgroup1.route1.POST');
                $subgroup->addMiddleware('group1.subgroup1.middleware1');
                $subgroup->addMiddleware('group1.subgroup1.middleware2');
            });
        $this->instance->addMiddleware('router.middleware3')->addMiddleware('router.middleware4')
                       ->group('/group2', function ($group) {
                           $group->addMiddleware('group2.middleware1');
                           $group->get('/route1', 'group2.route1.GET', 'group2.route1.GET');
                           $group->get('/route2', 'group2.route2.GET', 'group2.route2.GET');
                           $group->post('/route2', 'group2.route2.POST');
                           $group->addMiddleware('group2.middleware2');
                           $subgroup = $group->group('/subgroup1');
                           $subgroup->post('/route1', 'group2.subgroup1.route1.POST', 'group2.subgroup1.route1.POST');
                           $subgroup->addMiddleware('group2.subgroup1.middleware1');
                           $subgroup->addMiddleware('group2.subgroup1.middleware2');
                           $subgroup2 = $group->group('/subgroup2');
                           $subgroup2->any('/route1', 'group2.subgroup2.route1.ANY', 'group2.subgroup2.route1.ANY');
                           $subgroup2->addMiddleware('group2.subgroup2.middleware1');
                           $subgroup2->addMiddleware('group2.subgroup2.middleware2');
                       });
        $this->middlewares = [
            'router.middleware1'           => true,
            'router.middleware2'           => true,
            'router.middleware3'           => true,
            'router.middleware4'           => true,
            'group1.middleware1'           => false,
            'group1.middleware2'           => false,
            'group2.middleware1'           => false,
            'group2.middleware2'           => false,
            'group1.subgroup1.middleware1' => false,
            'group1.subgroup1.middleware2' => false,
            'group2.subgroup1.middleware1' => false,
            'group2.subgroup2.middleware1' => false,
            'group2.subgroup2.middleware2' => false,
        ];
        foreach ($this->middlewares as $middleware => $active) {
            $instance = new \Prometheus\Router\Spec\Middleware();
            $this->di->set($middleware, $instance);
        }
        $this->routes = [
            'group1.route1.GET',
            'group1.route2.GET',
            'group1.route2.POST',
            'group1.subgroup1.route1.POST',
            'group2.route1.GET',
            'group2.route2.GET',
            'group2.route2.POST',
            'group2.subgroup1.route1.POST',
            'group2.subgroup2.route1.ANY'
        ];
        foreach ($this->routes as $route) {
            $instance = new \Prometheus\Router\Spec\Handler();
            $this->di->set($route, $instance);
        }
    });
    beforeEach(function () {
    });
    it('handles group route with middlewares', function () {
        expectMiddlewares($this->di, $this->middlewares, [
            'group1.middleware1',
            'group1.middleware2'
        ]);
        expectRoutes($this->di, $this->routes, 'group1.route1.GET');
        expect($this->notFoundHandler)->not->toReceive('handle');
        expect($this->notAllowedHandler)->not->toReceive('handle');
        $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals(['REQUEST_URI' => '/group1/route1'], [], [], [], []);
        $this->instance->handle($request);
    });
    it('handles group route with middlewares again', function () {
        expectMiddlewares($this->di, $this->middlewares, [
            'group1.middleware1',
            'group1.middleware2'
        ]);
        expectRoutes($this->di, $this->routes, 'group1.route1.GET');
        expect($this->notFoundHandler)->not->toReceive('handle');
        expect($this->notAllowedHandler)->not->toReceive('handle');
        $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals(['REQUEST_URI' => '/group1/route1'], [], [], [], []);
        $this->instance->handle($request);
    });
    it('handles later group route with middlewares', function () {
        expectMiddlewares($this->di, $this->middlewares, [
            'group2.middleware1',
            'group2.middleware2'
        ]);
        expectRoutes($this->di, $this->routes, 'group2.route1.GET');
        expect($this->notFoundHandler)->not->toReceive('handle');
        expect($this->notAllowedHandler)->not->toReceive('handle');

        $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals(['REQUEST_URI' => '/group2/route1'], [], [], [], []);
        $this->instance->handle($request);
    });
    it('handles nested group route with middlewares', function () {
        expectMiddlewares($this->di, $this->middlewares, [
            'group1.middleware1',
            'group1.middleware2',
            'group1.subgroup1.middleware1',
            'group1.subgroup1.middleware2'
        ]);
        expectRoutes($this->di, $this->routes, 'group1.subgroup1.route1.POST');
        expect($this->notFoundHandler)->not->toReceive('handle');
        expect($this->notAllowedHandler)->not->toReceive('handle');

        $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
            'REQUEST_URI'    => '/group1/subgroup1/route1',
            'REQUEST_METHOD' => 'POST'
        ], [], [], [], []);
        $this->instance->handle($request);
    });
    it('builds routes', function () {
        expect($this->instance->pathFor('group1.route1.GET'))->toBe('/group1/route1');
        expect($this->instance->pathFor('group2.subgroup2.route1.ANY'))->toBe('/group2/subgroup2/route1');
    });
});

function expectRoutes($di, $routes, $acutal) {
    foreach ($routes as $route) {
        if ($route === $acutal) {
            expect($di->get($route))->toReceive('handle')->ordered;
        } else {
            expect($di->get($route))->not->toReceive('handle');
        }
    }
}

function expectMiddlewares($di, $middlewares, $enable) {
    foreach ($enable as $middleware) {
        $middlewares[ $middleware ] = true;
    }
    foreach ($middlewares as $middleware => $active) {
        if ($active) {
            expect($di->get($middleware))->toReceive('process')->ordered;
        } else {
            expect($di->get($middleware))->not->toReceive('process');
        }
    }
}