<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Prometheus\Router\RouteFactory;

describe(RouteFactory::class, function () {
    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $this->di         = $containerBuilder->build();
        $this->di->set('handlerFactory', $this->di);
    });
    beforeEach(function () {
        $this->instance = $this->di->get(RouteFactory::class);
    });
    it('implements the interface', function () {
        expect($this->instance)->toBeAnInstanceOf(\Prometheus\Router\RouteFactoryInterface::class);
    });
    it('creates Route', function () {
        expect($this->di->get(\Prometheus\Router\Middleware\GroupFactory::class))->toReceive('get');
        $route = $this->instance->createRoute(['GET'], '/test', 'test');
        expect($route)->toBeAnInstanceOf(\Prometheus\Router\Route::class);
        expect($route->getPattern())->toBe('/test');
        expect($route->getMethods())->toBe(['GET']);
        $reflection = new ReflectionClass($route);
        $property   = $reflection->getProperty('handler');
        $property->setAccessible(true);
        expect($property->getValue($route))->toBe('test');
        $property = $reflection->getProperty('middleware');
        $property->setAccessible(true);
        expect($property->getValue($route))->toBeAnInstanceOf(\Prometheus\Router\Middleware\Group::class);
    });
});