<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Kahlan\Plugin\Double;
use Prometheus\Router\Router;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

describe(Router::class, function () {

    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $this->handler    = Double::instance(['implements' => RequestHandlerInterface::class]);
        $containerBuilder->addDefinitions([
            Router::class                                 => \DI\autowire()
                ->constructorParameter('notFoundHandler', \DI\get(\Prometheus\Router\Handler\NotFound::class))
                ->constructorParameter('notAllowedHandler', \DI\get(\Prometheus\Router\Handler\MethodNotAllowed::class))
                ->constructorParameter('middleware', \DI\get('middleware'))->scope(\DI\Scope::PROTOTYPE),
            ResponseInterface::class                      => \DI\get(\Zend\Diactoros\Response\EmptyResponse::class),
            \Zend\Diactoros\Response\EmptyResponse::class => \DI\autowire()->scope(\DI\Scope::PROTOTYPE),
            'handler'                                     => $this->handler
        ]);
        $this->di = $containerBuilder->build();
        $this->di->set('handlerFactory', $this->di);
        $this->di->set('middlewareFactory', $this->di);
        $this->middleware = $this->di->get(\Prometheus\Router\Middleware\GroupFactory::class)
                                     ->get(\Prometheus\Router\Middleware\Group::class);
        $this->di->set('middleware', $this->middleware);
        $this->notFoundHandler   = $this->di->get(\Prometheus\Router\Handler\NotFound::class);
        $this->notAllowedHandler = $this->di->get(\Prometheus\Router\Handler\MethodNotAllowed::class);
    });
    beforeEach(function () {
        allow($this->handler)->toReceive('handle')
                             ->andReturn(Double::instance(['implements' => ResponseInterface::class]));
        $this->instance = $this->di->get(Router::class);
    });
    it('implements interfaces', function () {
        expect($this->instance)->toBeAnInstanceOf(\Prometheus\Router\RouteGenerationInterface::class);
        expect($this->instance)->toBeAnInstanceOf(\Prometheus\Router\Middleware\AwareInterface::class);
    });
    it('dispatches', function () {
        $this->instance->map(['GET'], '/test', $this->handler);
        $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals(['REQUEST_URI' => 'test'], [], [], [], []);
        expect($this->handler)->toReceive('handle');
        $this->instance->handle($request);
    });
    describe('HTTP Methods', function () {
        describe('::map', function () {
            it('creates route', function () {
                expect($this->instance->map(['GET'], '/pattern', $this->handler))->toBeAnInstanceOf(\Prometheus\Router\RouteInterface::class);
            });
            it('creates route from factory', function () {
                $routeFactory = $this->di->get(\Prometheus\Router\RouteFactory::class);
                expect($routeFactory)->toReceive('createRoute')->with(['GET', 'POST'], '/pattern2', $this->handler);
                $this->instance->map(['GET', 'POST'], '/pattern2', $this->handler, 'name');
            });
            it('adds middleware', function () {
                $routeFactory = $this->di->get(\Prometheus\Router\RouteFactory::class);
                $route        = $routeFactory->createRoute(['GET'], '', $this->handler);
                allow($routeFactory)->toReceive('createRoute')->andReturn($route);
                expect($route)->toReceive('addMiddleware')->with($this->middleware);
                $this->instance->map(['GET'], '', $this->handler);
            });
            it('throws Exception for same route', function () {
                $this->instance->get('/test', $this->handler, 'test');
                $handler  = $this->handler;
                $instance = $this->instance;
                $closure  = function () use ($instance, $handler) {
                    $instance->get('/test', $handler, 'test');
                };
                expect($closure)->toThrow(new Prometheus\Router\Exception\Route('Route-name test already in use'));
            });
        });
        describe('::get', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['GET'], '/pattern2', $this->handler);
                $this->instance->get('/pattern2', $this->handler);
            });
        });
        describe('::post', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['POST'], '/pattern2', $this->handler);
                $this->instance->post('/pattern2', $this->handler);
            });
        });
        describe('::put', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['PUT'], '/pattern2', $this->handler);
                $this->instance->put('/pattern2', $this->handler);
            });
        });
        describe('::patch', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['PATCH'], '/pattern2', $this->handler);
                $this->instance->patch('/pattern2', $this->handler);
            });
        });
        describe('::delete', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['DELETE'], '/pattern2', $this->handler);
                $this->instance->delete('/pattern2', $this->handler);
            });
        });
        describe('::options', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with(['OPTIONS'], '/pattern2', $this->handler);
                $this->instance->options('/pattern2', $this->handler);
            });
        });
        describe('::any', function () {
            it('proxies map', function () {
                expect($this->instance)->toReceive('map')->with([
                    'GET',
                    'POST',
                    'PUT',
                    'PATCH',
                    'DELETE',
                    'OPTIONS'
                ], '/pattern2', $this->handler);
                $this->instance->any('/pattern2', $this->handler);
            });
        });
        it('handles PUT', function () {
            $this->instance->put('/test', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'PUT'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
        });
        it('handles HEAD as GET', function () {
            $this->instance->get('/test', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'HEAD'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
        });
        it('handles POST', function () {
            $this->instance->post('/test', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'POST'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
        });
        it('handles DELETE', function () {
            $this->instance->delete('/test', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'DELETE'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
        });
        it('handles PATCH', function () {
            $this->instance->patch('/test', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'PATCH'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
        });
        it('handles OPTIONS', function () {
            $this->instance->options('/test', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => '/test',
                'REQUEST_METHOD' => 'OPTIONS'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
        });
        it('handles any', function () {
            $this->instance->any('/test', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'OPTIONS'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'PUT'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'GET'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'HEAD'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'POST'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'DELETE'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => 'test',
                'REQUEST_METHOD' => 'PATCH'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
        });
    });
    it('handles not allowed methods', function () {
        $this->instance->map(['GET', 'POST'], '/test', $this->handler);
        $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
            'REQUEST_URI'    => '/test',
            'REQUEST_METHOD' => 'OPTIONS'
        ], [], [], [], []);
        expect($this->handler)->not->toReceive('handle');
        $handler = $this->di->get(\Prometheus\Router\Handler\MethodNotAllowed::class);
        expect($handler)->toReceive('handle')->with(\Kahlan\Arg::toMatch(function ($expected) {
            return $expected->getAttribute('allowedMethods') === ['GET', 'POST'];
        }));
        $this->instance->handle($request);
    });
    it('handles unknown routes', function () {
        $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
            'REQUEST_URI' => '/test'
        ], [], [], [], []);
        expect($this->handler)->not->toReceive('handle');
        $handler = $this->di->get(\Prometheus\Router\Handler\NotFound::class);
        expect($handler)->toReceive('handle');
        $this->instance->handle($request);
    });
    describe('routes', function () {
        it('handles multiple routes', function () {
            $this->instance->get('/test', $this->handler);
            $handler2 = clone $this->handler;
            allow($handler2)->toReceive('handle')
                            ->andReturn(Double::instance(['implements' => ResponseInterface::class]));
            $this->instance->get('/test2', $handler2);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI' => '/test'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI' => '/test2'
            ], [], [], [], []);
            expect($handler2)->toReceive('handle');
            $this->instance->handle($request);
        });
        it('handles multiple routes with same path', function () {
            $this->instance->get('/test', $this->handler);
            $handler2 = clone $this->handler;
            allow($handler2)->toReceive('handle')
                            ->andReturn(Double::instance(['implements' => ResponseInterface::class]));
            $this->instance->post('/test', $handler2);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI' => '/test'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI'    => '/test',
                'REQUEST_METHOD' => 'POST'
            ], [], [], [], []);
            expect($handler2)->toReceive('handle');
            $this->instance->handle($request);
        });
        it('adds route arguments', function () {
            $this->instance->get('/test/{arg1}/{arg2}/{arg3}', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI' => '/test/a/b/1'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle')->with(\Kahlan\Arg::toMatch(function ($expected) {
                return $expected->getAttributes() === [
                        'arg1' => 'a',
                        'arg2' => 'b',
                        'arg3' => '1',
                    ];
            }));
            $this->instance->handle($request);
        });
        it('allows route arguments to be optional', function () {
            $this->instance->get('/test/{arg1}/{arg2}[/{arg3}]', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI' => '/test/a/b'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle')->with(\Kahlan\Arg::toMatch(function ($expected) {
                return $expected->getAttributes() === [
                        'arg1' => 'a',
                        'arg2' => 'b',
                    ];
            }));
            $this->instance->handle($request);
        });
        it('allows route arguments to match expressions', function () {
            $this->instance->get('/test/{arg1:[a-z]}', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI' => '/test/a'
            ], [], [], [], []);
            expect($this->handler)->toReceive('handle')->with(\Kahlan\Arg::toMatch(function ($expected) {
                return $expected->getAttributes() === [
                        'arg1' => 'a',
                    ];
            }));
            $this->instance->handle($request);
        });
        it('ignores mismatched routes', function () {
            $this->instance->get('/test/{arg1:[a-z]}', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([
                'REQUEST_URI' => '/test/A'
            ], [], [], [], []);
            expect($this->handler)->not->toReceive('handle');
            $handler = $this->di->get(\Prometheus\Router\Handler\NotFound::class);
            expect($handler)->toReceive('handle');
            $this->instance->handle($request);
        });
    });
    describe('::group', function () {
        beforeAll(function () {
            $this->groupFactory      = $this->di->get(\Prometheus\Router\GroupFactory::class);
            $this->middlewareFactory = $this->di->get(\Prometheus\Router\Middleware\GroupFactory::class);
        });
        it('creates group using factory', function () {
            $group = $this->groupFactory->createGroup($this->instance, 'pattern');
            allow($this->groupFactory)->toReceive('createGroup')->with($this->instance, 'pattern')->andReturn($group);
            expect($this->groupFactory)->toReceive('createGroup')->with($this->instance, 'pattern');
            expect($this->instance->group('pattern'))->toBe($group);
        });
        it('executes callback', function () {
            $group = $this->groupFactory->createGroup($this->instance, '');
            allow($this->groupFactory)->toReceive('createGroup')->andReturn($group);
            $test     = false;
            $callback = function ($actual) use ($group, &$test) {
                expect($actual)->toBe($group);
                $test = true;
            };
            expect($this->instance->group('pattern', $callback))->toBe($group);
            expect($test)->toBe(true);
        });
        it('matches group routes', function () {
            $group = $this->instance->group('/test');
            $group->get('/test', $this->handler);
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals(['REQUEST_URI' => '/test/test'], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
        });
        it('adds group routes using callback', function () {
            $handler = $this->handler;
            $this->instance->group('/test', function ($group) use ($handler) {
                $group->get('/test', $handler);
            });
            $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals(['REQUEST_URI' => '/test/test'], [], [], [], []);
            expect($this->handler)->toReceive('handle');
            $this->instance->handle($request);
        });
    });
    describe('::pathFor', function () {
        it('throws Exception for unknown route', function () {
            $instance = $this->instance;
            $closure  = function () use ($instance) {
                $instance->pathFor('test');
            };
            expect($closure)->toThrow(new \Prometheus\Router\Exception\Route('Named route does not exist for name: test'));
        });
        it('returns path for route', function () {
            $this->instance->get('/test', $this->handler, 'test');
            expect($this->instance->pathFor('test'))->toBe('/test');
        });
        it('returns path with attributes', function () {
            $this->instance->get('/test/{arg1}', $this->handler, 'test');
            expect($this->instance->pathFor('test', ['arg1' => 'a']))->toBe('/test/a');
        });
        it('throws Exception for missing attribute', function () {
            $this->instance->get('/test/{arg1}', $this->handler, 'test');
            $instance = $this->instance;
            $closure  = function () use ($instance) {
                $instance->pathFor('test');
            };
            expect($closure)->toThrow(new InvalidArgumentException('Missing data for URL segment: arg1'));
        });
    });
});