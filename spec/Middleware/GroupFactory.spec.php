<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Prometheus\Router\Middleware\GroupFactory;

describe(GroupFactory::class, function () {
    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $this->di         = $containerBuilder->build();
        $this->di->set('queueFactory', $this->di);
    });
    beforeEach(function () {
        $this->instance = $this->di->get(GroupFactory::class);
    });
    it('implements interfaces', function () {
        expect($this->instance)->toBeAnInstanceOf(\Psr\Container\ContainerInterface::class);
    });
    describe('::has', function () {
        it('returns true for Group', function () {
            expect($this->instance->has(\Prometheus\Router\Middleware\Group::class))->toBe(true);
        });
        it('returns false for anything else', function () {
            expect($this->instance->has('group'))->toBe(false);
            expect($this->instance->has(password_hash('some thing', PASSWORD_DEFAULT)))->toBe(false);
        });
    });
    describe('::get', function () {
        it('returns Group instance', function () {
            $result = $this->instance->get(\Prometheus\Router\Middleware\Group::class);
            expect($result)->toBeAnInstanceOf(\Prometheus\Router\Middleware\Group::class);
        });
        it('throws exception for anything else', function () {
            $instance = $this->instance;
            $closure  = function () use ($instance) {
                $instance->get('test');
            };
            expect($closure)->toThrow(new \Prometheus\Router\Exception\NotFound('Unknown identifier test'));
        });
    });
});