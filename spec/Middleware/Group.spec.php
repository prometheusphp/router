<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Kahlan\Plugin\Double;
use Prometheus\Router\Middleware\Group;
use Psr\Http\Server\RequestHandlerInterface;

describe(Group::class, function () {
    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $containerBuilder->addDefinitions([]);
        $this->di      = $containerBuilder->build();
        $this->request = \Zend\Diactoros\ServerRequestFactory::fromGlobals([], [], [], [], []);
        $this->handler = Double::instance(['implements' => RequestHandlerInterface::class]);
    });
    beforeEach(function () {
        $this->queue = new \Prometheus\Router\Middleware\Queue();
        $this->di->set(\Prometheus\Router\Middleware\Queue::class, $this->queue);
        $this->instance = new Group($this->di);
        allow($this->handler)->toReceive('handle')
                             ->andReturn(Double::instance(['implements' => \Psr\Http\Message\ResponseInterface::class]));
    });
    it('implements interfaces', function () {
        expect($this->instance)->toBeAnInstanceOf(\Prometheus\Router\Middleware\CapableInterface::class);
        expect($this->instance)->toBeAnInstanceOf(\Psr\Http\Server\MiddlewareInterface::class);
    });
    describe('::getQueue', function () {
        it('calls queueFactory only once', function () {
            expect($this->di)->toReceive('get')->with(\Prometheus\Router\Middleware\Queue::class)->once();
            $this->instance->addMiddleware('test');
            $this->instance->addMiddleware('test');
        });
    });
    describe('::addMiddleware', function () {
        it('proxies queue', function () {
            $name = 'test';
            expect($this->queue)->toReceive('addMiddleware')->with($name);
            $this->instance->addMiddleware($name);
        });
    });
    describe('::process', function () {
        it('passes to handler without middlewares', function () {
            expect($this->queue)->not->toReceive('handle');
            expect($this->handler)->toReceive('handle');
            $result = $this->instance->process($this->request, $this->handler);
            expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
        });
        it('sets handler on queue and runs queue', function () {
            $middleware = Double::instance(['implements' => \Psr\Http\Server\MiddlewareInterface::class]);
            allow($middleware)->toReceive('process')
                              ->andRun(function (\Psr\Http\Message\ServerRequestInterface $request,
                                                 RequestHandlerInterface $handler) {
                                  return $handler->handle($request);
                              });
            $this->instance->addMiddleware($middleware);
            expect($this->queue)->toReceive('setHandler')->with($this->handler);
            expect($this->queue)->toReceive('handle')->with($this->request);

            $result = $this->instance->process($this->request, $this->handler);
            expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
        });
    });
});