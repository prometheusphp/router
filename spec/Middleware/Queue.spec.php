<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Kahlan\Plugin\Double;
use Prometheus\Router\Middleware\Queue;
use Psr\Http\Server\RequestHandlerInterface;

describe(Queue::class, function () {
    beforeAll(function () {
        $containerBuilder = clone($this->containerBuilder);
        $this->di         = $containerBuilder->build();
        $this->request    = \Zend\Diactoros\ServerRequestFactory::fromGlobals([], [], [], [], []);
        $this->handler    = Double::instance(['implements' => RequestHandlerInterface::class]);
    });
    beforeEach(function () {
        $this->instance = new Queue();
        $this->instance->setHandler($this->handler);
        $this->middleware = buildMiddleware();
        allow($this->handler)->toReceive('handle')
                             ->andReturn(Double::instance(['implements' => \Psr\Http\Message\ResponseInterface::class]));
    });
    it('implements interfaces', function () {
        expect($this->instance)->toBeAnInstanceOf(\Psr\Http\Server\RequestHandlerInterface::class);
        expect($this->instance)->toBeAnInstanceOf(\Prometheus\Router\Middleware\AwareInterface::class);
    });
    it('dispatches handler without any middlewares', function () {
        expect($this->handler)->toReceive('handle')->with($this->request);
        $result = $this->instance->handle($this->request);
        expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
    });
    it('dispatches middleware', function () {
        $this->instance->addMiddleware($this->middleware);
        expect($this->middleware)->toReceive('process')->with($this->request);
        expect($this->handler)->toReceive('handle')->with($this->request);
        $result = $this->instance->handle($this->request);
        expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
    });
    it('dispatches multiple middlewares', function () {
        $this->instance->addMiddleware($this->middleware);
        $middleware2 = buildMiddleware();
        $this->instance->addMiddleware($middleware2);
        expect($this->middleware)->toReceive('process')->with($this->request);
        expect($middleware2)->toReceive('process')->with($this->request)->ordered;
        expect($this->handler)->toReceive('handle')->with($this->request);
        $result = $this->instance->handle($this->request);
        expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
    });
    it('runs multiple times with middlewares', function () {
        $this->instance->addMiddleware($this->middleware);
        $middleware2 = buildMiddleware();
        $this->instance->addMiddleware($middleware2);
        expect($this->middleware)->toReceive('process')->with($this->request);
        expect($middleware2)->toReceive('process')->with($this->request)->ordered;
        expect($this->handler)->toReceive('handle')->with($this->request);
        $result = $this->instance->handle($this->request);
        expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);

        expect($this->middleware)->toReceive('process')->with($this->request);
        expect($middleware2)->toReceive('process')->with($this->request)->ordered;
        expect($this->handler)->toReceive('handle')->with($this->request);
        $result = $this->instance->handle($this->request);
        expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
    });
    it('prevents adding middleware during dispatch', function () {
        $middleware = Double::instance(['implements' => \Psr\Http\Server\MiddlewareInterface::class]);
        allow($middleware)->toReceive('process')->andRun(function (\Psr\Http\Message\ServerRequestInterface $request,
                                                                   RequestHandlerInterface $handler) {
            $handler->addMiddleware('test');

            return $handler->handle($request);
        });
        $instance = $this->instance;
        $instance->addMiddleware($middleware);
        $request = $this->request;
        $closure = function () use ($instance, $request) {
            $instance->handle($request);
        };
        expect($closure)->toThrow(new \RuntimeException('Can not add middleware after dispatch.'));
    });
    it('prevents adding middleware after dispatch', function () {
        $this->instance->handle($this->request);
        $instance = $this->instance;
        $closure  = function () use ($instance) {
            $instance->addMiddleware('test');
        };
        expect($closure)->toThrow(new \RuntimeException('Can not add middleware after dispatch.'));
    });
    describe('::addMiddleware', function () {
        it('errors on invalid type', function () {
            $instance = $this->instance;
            $closure  = function () use ($instance) {
                $instance->addMiddleware(1);
            };
            expect($closure)->toThrow(new InvalidArgumentException(sprintf('Middleware has to be a string or an instance of %s, integer given', \Psr\Http\Server\MiddlewareInterface::class)));
            $closure = function () use ($instance) {
                $instance->addMiddleware(new stdClass());
            };
            expect($closure)->toThrow(new InvalidArgumentException(sprintf('Middleware has to be a string or an instance of %s, Instance of stdClass given', \Psr\Http\Server\MiddlewareInterface::class)));
        });
        it('accepts strings and middlewares', function () {
            $instance = $this->instance;
            $closure  = function () use ($instance) {
                $instance->addMiddleware('test');
            };
            expect($closure)->not->toThrow();
            $closure = function () use ($instance) {
                $instance->addMiddleware(Double::instance(['implements' => \Psr\Http\Server\MiddlewareInterface::class]));
            };
            expect($closure)->not->toThrow();
        });
    });
    describe('::setHandler', function () {
        it('errors on invalid type', function () {
            $instance = $this->instance;
            $closure  = function () use ($instance) {
                $instance->setHandler(1);
            };
            expect($closure)->toThrow(new InvalidArgumentException(sprintf('Handler has to be a string or an instance of %s, integer given', RequestHandlerInterface::class)));
            $closure = function () use ($instance) {
                $instance->setHandler(new stdClass());
            };
            expect($closure)->toThrow(new InvalidArgumentException(sprintf('Handler has to be a string or an instance of %s, Instance of stdClass given', RequestHandlerInterface::class)));
        });
        it('accepts strings and middlewares', function () {
            $instance = $this->instance;
            $closure  = function () use ($instance) {
                $instance->setHandler('test');
            };
            expect($closure)->not->toThrow();
            $closure = function () use ($instance) {
                $instance->setHandler(Double::instance(['implements' => RequestHandlerInterface::class]));
            };
            expect($closure)->not->toThrow();
        });
    });
    describe('factories', function () {
        it('fails on unresolved middleware', function () {
            $name = 'test';
            $this->instance->addMiddleware($name);
            $this->instance->setHandler($this->handler);
            expect($this->middleware)->not->toReceive('process')->with($this->request);
            expect($this->handler)->not->toReceive('handle')->with($this->request);
            $instance = $this->instance;
            $request  = $this->request;
            $closure  = function () use ($instance, $request) {
                $instance->handle($request);
            };
            expect($closure)->toThrow(new RuntimeException(sprintf('Middleware was not resolved to an instance of %s, string given', \Psr\Http\Server\MiddlewareInterface::class)));
        });
        it('fails on incorrect resolved middleware', function () {
            $name = 'test';
            $this->di->set($name, new stdClass());
            $instance = new Queue(null, $this->di);
            $instance->addMiddleware($name);
            $instance->setHandler($this->handler);
            expect($this->middleware)->not->toReceive('process')->with($this->request);
            expect($this->handler)->not->toReceive('handle')->with($this->request);
            $request = $this->request;
            $closure = function () use ($instance, $request) {
                $instance->handle($request);
            };
            expect($closure)->toThrow(new RuntimeException(sprintf('Middleware was not resolved to an instance of %s, instance of stdClass given', \Psr\Http\Server\MiddlewareInterface::class)));
        });
        it('fails on incorrect resolved handler', function () {
            $name = 'test';
            $this->di->set($name, new stdClass());
            $instance = new Queue($this->di, null);
            $instance->addMiddleware($this->middleware);
            $instance->setHandler($name);
            expect($this->middleware)->not->toReceive('process')->with($this->request);
            expect($this->handler)->not->toReceive('handle')->with($this->request);
            $request = $this->request;
            $closure = function () use ($instance, $request) {
                $instance->handle($request);
            };
            expect($closure)->toThrow(new RuntimeException(sprintf('Handler was not resolved to an instance of %s, instance of stdClass given', RequestHandlerInterface::class)));
        });
        it('fails on unresolved handler', function () {
            $name = 'test';
            $this->instance->addMiddleware($this->middleware);
            $this->instance->setHandler($name);
            expect($this->middleware)->not->toReceive('process')->with($this->request);
            expect($this->handler)->not->toReceive('handle')->with($this->request);
            $instance = $this->instance;
            $request  = $this->request;
            $closure  = function () use ($instance, $request) {
                $instance->handle($request);
            };
            expect($closure)->toThrow(new RuntimeException(sprintf('Handler was not resolved to an instance of %s, string given', RequestHandlerInterface::class)));
        });
        it('uses middlewareFactory to resolve middleware', function () {
            $name = 'test';
            $this->di->set($name, $this->middleware);
            $instance = new Queue(null, $this->di);
            $instance->addMiddleware($name);
            $instance->setHandler($this->handler);
            expect($this->middleware)->toReceive('process')->with($this->request);
            expect($this->handler)->toReceive('handle')->with($this->request);
            $result = $instance->handle($this->request);
            expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
        });
        it('uses handlerFactory to resolve handler', function () {
            $name = 'test';
            $this->di->set($name, $this->handler);
            $instance = new Queue($this->di, null);
            $instance->addMiddleware($this->middleware);
            $instance->setHandler($name);
            expect($this->middleware)->toReceive('process')->with($this->request);
            expect($this->handler)->toReceive('handle')->with($this->request);
            $result = $instance->handle($this->request);
            expect($result)->toBeAnInstanceOf(\Psr\Http\Message\ResponseInterface::class);
        });
    });
});

function buildMiddleware() {
    $middleware = Double::instance(['implements' => \Psr\Http\Server\MiddlewareInterface::class]);
    allow($middleware)->toReceive('process')->andRun(function (\Psr\Http\Message\ServerRequestInterface $request,
                                                               RequestHandlerInterface $handler) {
        return $handler->handle($request);
    });

    return $middleware;
}